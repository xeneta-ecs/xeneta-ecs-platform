####################################################################
# Network configurations
region               = "us-east-1"
vpc_cidr_block       = "10.0.0.0/16"
enable_dns_hostnames = false
vpc_name             = "ecs-vpc"
namespace_name       = "xeneta.local"
instance_tenancy     = "default"

subnet_cidrs = {
  public_subnet_cidr = [
    {
      cidr_range        = "10.0.0.0/24"
      availability_zone = "us-east-1a"
      name              = "public-subnet-ue-1a",
      tags = {
        "type" = "public"
      }
    },
    {
      cidr_range        = "10.0.1.0/24"
      availability_zone = "us-east-1b"
      name              = "public-subnet-ue-1b"
      tags = {
        "type" = "public"
      }
    },
    {
      cidr_range        = "10.0.2.0/24"
      availability_zone = "us-east-1c"
      name              = "public-subnet-ue-1c"
      tags = {
        "type" = "public"
      }
    }
  ]
  private_subnet_cidr = [
    {
      cidr_range        = "10.0.3.0/24"
      availability_zone = "us-east-1a"
      name              = "private-subnet-ue-1a"
      tags = {
        "type" = "private"
      }
    },
    {
      cidr_range        = "10.0.4.0/24"
      availability_zone = "us-east-1b"
      name              = "private-subnet-ue-1b"
      tags = {
        "type" = "private"
      }
    },
    {
      cidr_range        = "10.0.5.0/24"
      availability_zone = "us-east-1c"
      name              = "private-subnet-ue-1c"
      tags = {
        "type" = "private"
      }
    }
  ]
  database_subnet_cidr = [
    {
      cidr_range        = "10.0.6.0/24"
      availability_zone = "us-east-1a"
      name              = "database-subnet-ue-1a"
      tags = {
        "type" = "database"
      }
    },
    {
      cidr_range        = "10.0.7.0/24"
      availability_zone = "us-east-1b"
      name              = "database-subnet-ue-1b"
      tags = {
        "type" = "database"
      }
    },
    {
      cidr_range        = "10.0.8.0/24"
      availability_zone = "us-east-1c"
      name              = "database-subnet-ue-1c"
      tags = {
        "type" = "database"
      }
    }
  ]
}

security_group_rule = {
  public = [
    {
      type                     = "ingress"
      from_port                = 443
      to_port                  = 443
      protocol                 = "tcp"
      cidr_blocks              = ["0.0.0.0/0"]
      ipv6_cidr_blocks         = ["::/0"]
      description              = "allow https inbound"
      self                     = null
      source_security_group_id = null
    },
    {
      type                     = "ingress"
      from_port                = 80
      to_port                  = 80
      protocol                 = "tcp"
      cidr_blocks              = ["0.0.0.0/0"]
      ipv6_cidr_blocks         = ["::/0"]
      description              = "allow http inbound"
      self                     = null
      source_security_group_id = null
    },
    {
      type                     = "egress"
      from_port                = 0
      to_port                  = 0
      protocol                 = "-1"
      cidr_blocks              = ["0.0.0.0/0"]
      ipv6_cidr_blocks         = null
      description              = "allow all outbound"
      self                     = null
      source_security_group_id = null
    }
  ]
  private = [
    {
      type                     = "ingress"
      from_port                = 3000
      to_port                  = 3000
      protocol                 = "tcp"
      source_security_group_id = "public"
      cidr_blocks              = null
      ipv6_cidr_blocks         = null
      description              = "allow 3000 inbound"
      self                     = null
    },
    {
      type                     = "egress"
      from_port                = 0
      to_port                  = 0
      protocol                 = "-1"
      cidr_blocks              = ["0.0.0.0/0"]
      ipv6_cidr_blocks         = null
      description              = "allow all outbound"
      self                     = null
      source_security_group_id = null
    }
  ]
  database = [
    {
      type                     = "ingress"
      from_port                = 5432
      to_port                  = 5432
      protocol                 = "tcp"
      source_security_group_id = "private"
      cidr_blocks              = null
      ipv6_cidr_blocks         = null
      description              = "allow 5432 inbound"
      self                     = null
    }
  ]
}
