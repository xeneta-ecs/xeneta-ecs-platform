# VPC, subnets, route tables
module "core-network" {
  source               = "./services/core-network/"
  region               = var.region
  vpc_cidr_block       = var.vpc_cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames
  vpc_name             = local.vpc_name
  instance_tenancy     = var.instance_tenancy
  resource_prefix      = local.resource_prefix
  subnet_cidrs         = var.subnet_cidrs
  namespace_name       = var.namespace_name
  namespace_tags       = local.namespace_tags
}

# subnet, route tables, Routes, SGs, NGW, IGW, EIPs associations, 
module "core-connectivity" {
  source                       = "./services/connectivity"
  vpc_id                       = module.core-network.vpc_id
  resource_prefix              = local.resource_prefix
  subnet_cidrs                 = var.subnet_cidrs
  public_subnet_details        = module.core-network.public_subnet_list[0]
  private_subnet_details       = module.core-network.private_subnet_list[0]
  database_subnet_details      = module.core-network.database_subnet_list[0]
  public_route_table_details   = module.core-network.public_route_table_ids
  private_route_table_details  = module.core-network.private_route_table_ids
  database_route_table_details = module.core-network.database_route_table_ids
  security_group_rule          = var.security_group_rule
}