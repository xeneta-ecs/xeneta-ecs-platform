####################################################################
# Networking attributes
variable "region" {
  description = "Region where we need network created in"
  type        = string
}

variable "vpc_cidr_block" {
  description = "VPC CIDR block"
  type        = string
}

variable "enable_dns_hostnames" {
  description = "Enable DNS host name or not"
  type        = bool
}

variable "vpc_name" {
  description = "Name of the ECS VPC"
  type        = string
}

variable "namespace_name" {
  description = "Private hosted zone for service discovery"
  type        = string
}

variable "instance_tenancy" {
  description = "Instance tenancy within the VPC"
  type        = string
}

variable "subnet_cidrs" {
  description = "CIDRs for the subnets to be created"
  type        = map(any)

}

variable "security_group_rule" {
  description = "values for the security group rules to be created"
}

