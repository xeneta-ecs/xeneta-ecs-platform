output "subnet_id_list" {
  value = { for k, v in aws_security_group_rule.managed_sg_rule : k => v }
}