#required
variable "type" {
  description = "Type of rule being created. Valid options are ingress (inbound) or egress (outbound)."
}

variable "from_port" {
  description = "Start port"
}

variable "to_port" {
  description = "End port"
}

variable "protocol" {
  description = "Protocol. If not icmp, icmpv6, tcp, udp, or all use the protocol number"
}
variable "security_group_id" {
  default = null

}

#optional
variable "cidr_blocks" {
  default = null

}
variable "ipv6_cidr_blocks" {
  default = null

}
variable "description" {
  default = null

}
variable "prefix_list_ids" {
  default = null

}
variable "self" {
  default = null

}
variable "source_security_group_id" {
  default = null

}