output "subnet_id_list" {
  value = { for k, v in aws_subnet.subnet : v.tags.Name => v.id }
}
