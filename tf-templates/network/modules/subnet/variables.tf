variable "subnet_list" {
  description = "list of subnet cidrs to be created"
}

variable "vpc_id" {
  description = "ID of the VPC where subnet is created"
}

variable "resource_prefix" {
  description = "resource prefix"
}

variable "private_rt_id" {
  default = null
}

variable "public_rt_id" {
  default = null

}