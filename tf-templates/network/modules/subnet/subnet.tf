resource "aws_subnet" "subnet" {
  for_each                = { for subnet in var.subnet_list : subnet.cidr_range => subnet }
  vpc_id                  = var.vpc_id
  cidr_block              = each.value["cidr_range"]
  map_public_ip_on_launch = contains(split("-", each.value["name"]), "public") ? true : false
  availability_zone       = each.value["availability_zone"]
  tags = merge(each.value["tags"], {
    Name = format("%s-%s", var.resource_prefix, each.value["name"])
  })
}