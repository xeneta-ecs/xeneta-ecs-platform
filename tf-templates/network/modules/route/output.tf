output "aws_route_details" {
  value = { for k, v in aws_route.managed_route : k => v }
}