output "nat_gateway_details" {
  value = local.nat_gateway_details
}
output "security_group_details" {
  value = local.security_group_details
}