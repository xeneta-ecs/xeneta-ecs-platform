#IGW for internet connectivty
resource "aws_internet_gateway" "public_subnet_igw" {
  vpc_id = var.vpc_id

  tags = {
    Name = format("%s-xeneta-igw", var.resource_prefix)
  }
}

# Elastic IP for the Nat GW (3 eips since there are 3 public subnets having 3 nagws)
resource "aws_eip" "nat_gw_eip" {
  for_each = { for subnet in var.subnet_cidrs["public_subnet_cidr"] : subnet.cidr_range => subnet }
  vpc      = true
  tags = {
    Name = format("%s-%s-natgw-eip", var.resource_prefix, each.value["name"])
  }

  depends_on = [aws_internet_gateway.public_subnet_igw]
}

# Nat gateways for outbound connection for private subnets (3 natgws for each AZ for high availability)
resource "aws_nat_gateway" "nat_gateway" {
  for_each      = aws_eip.nat_gw_eip
  allocation_id = each.value.id
  subnet_id     = lookup(var.public_subnet_details, replace(each.value.tags.Name, "-natgw-eip", ""))
  tags = {
    Name = format("%s-%s-%s-natgw", var.resource_prefix, split("-", each.value.tags.Name)[5], split("-", each.value.tags.Name)[6])
  }

}

# ADD outbound internet connectivity rules to private subnet route tables
resource "aws_route" "private_subnet_internet_access" {
  count                  = length(flatten(values(local.nat_gateway_details)))
  route_table_id         = values(var.private_route_table_details)[count.index]
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = values(local.nat_gateway_details)[count.index]
}

# ADD outbound internet connectivity rules to public subnet route tables
resource "aws_route" "public_subnet_internet_access" {
  for_each               = var.public_route_table_details
  route_table_id         = each.value
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.public_subnet_igw.id
}

# create SGs 
resource "aws_security_group" "access_security_groups" {
  for_each    = var.subnet_cidrs
  name        = format("%s-%s-security-group", var.resource_prefix, split("_", each.key)[0])
  description = format("security-group for the %s tier", split("_", each.key)[0])
  vpc_id      = var.vpc_id

  tags = {
    Name = format("%s-%s-security-group", var.resource_prefix, split("_", each.key)[0])
    type = split("_", each.key)[0]
  }
}


# Create subnet - route table associations
resource "aws_route_table_association" "public-route-table-association" {
  for_each       = local.public_subnet_ids_indexedwith_az
  subnet_id      = each.value
  route_table_id = lookup(local.public_rt_ids_indexedwith_az, each.key)
}

resource "aws_route_table_association" "private-route-table-association" {
  for_each       = local.private_subnet_ids_indexedwith_az
  subnet_id      = each.value
  route_table_id = lookup(local.private_rt_ids_indexedwith_az, each.key)
}

resource "aws_route_table_association" "database-route-table-association" {
  for_each       = local.database_subnet_ids_indexedwith_az
  subnet_id      = each.value
  route_table_id = lookup(local.database_rt_ids_indexedwith_az, each.key)
}

# public security group rules 
module "public_security_group_rules" {
  source                   = "../../modules/security_group_rule/"
  for_each                 = { for rule in var.security_group_rule["public"] : rule.description => rule }
  type                     = each.value["type"]
  from_port                = each.value["from_port"]
  to_port                  = each.value["to_port"]
  protocol                 = each.value["protocol"]
  security_group_id        = lookup(local.security_group_details, format("%s-public-security-group", var.resource_prefix))
  cidr_blocks              = each.value["cidr_blocks"]
  ipv6_cidr_blocks         = each.value["ipv6_cidr_blocks"]
  description              = each.value["description"]
  self                     = each.value["self"]
  source_security_group_id = each.value["source_security_group_id"] != null ? lookup(local.security_group_details, format("%s-%s-security-group", var.resource_prefix, each.value["source_security_group_id"])) : null
}


# private security group rules 
module "private_security_group_rules" {
  source                   = "../../modules/security_group_rule/"
  for_each                 = { for rule in var.security_group_rule["private"] : rule.description => rule }
  type                     = each.value["type"]
  from_port                = each.value["from_port"]
  to_port                  = each.value["to_port"]
  protocol                 = each.value["protocol"]
  security_group_id        = lookup(local.security_group_details, format("%s-private-security-group", var.resource_prefix))
  cidr_blocks              = each.value["cidr_blocks"]
  ipv6_cidr_blocks         = each.value["ipv6_cidr_blocks"]
  description              = each.value["description"]
  self                     = each.value["self"]
  source_security_group_id = each.value["source_security_group_id"] != null ? lookup(local.security_group_details, format("%s-%s-security-group", var.resource_prefix, each.value["source_security_group_id"])) : null
}


# databse security group rules 
module "databse_security_group_rules" {
  source                   = "../../modules/security_group_rule/"
  for_each                 = { for rule in var.security_group_rule["database"] : rule.description => rule }
  type                     = each.value["type"]
  from_port                = each.value["from_port"]
  to_port                  = each.value["to_port"]
  protocol                 = each.value["protocol"]
  security_group_id        = lookup(local.security_group_details, format("%s-database-security-group", var.resource_prefix))
  cidr_blocks              = each.value["cidr_blocks"]
  ipv6_cidr_blocks         = each.value["ipv6_cidr_blocks"]
  description              = each.value["description"]
  self                     = each.value["self"]
  source_security_group_id = each.value["source_security_group_id"] != null ? lookup(local.security_group_details, format("%s-%s-security-group", var.resource_prefix, each.value["source_security_group_id"])) : null
}