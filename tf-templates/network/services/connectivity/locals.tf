locals {
  nat_gateway_details = { for k, v in aws_nat_gateway.nat_gateway : v.tags.Name => v.id }

  security_group_details = { for k, v in aws_security_group.access_security_groups : v.name => v.id }

  public_subnet_ids_indexedwith_az = { for k, v in var.public_subnet_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

  private_subnet_ids_indexedwith_az = { for k, v in var.private_subnet_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

  database_subnet_ids_indexedwith_az = { for k, v in var.database_subnet_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

  public_rt_ids_indexedwith_az = { for k, v in var.public_route_table_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

  private_rt_ids_indexedwith_az = { for k, v in var.private_route_table_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

  database_rt_ids_indexedwith_az = { for k, v in var.database_route_table_details : format("%s-%s", split("-", k)[5], split("-", k)[6]) => v }

}