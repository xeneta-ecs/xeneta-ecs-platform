variable "vpc_id" {
  description = "Name of the ECS VPC"
  type        = string
}

variable "resource_prefix" {
  description = "prefix for the reources to be created"
  type        = string
}

variable "subnet_cidrs" {
  description = "CIDRs for the subnets to be created"
}

variable "public_subnet_details" {
  description = "List of cidrs for public subnets"
}

variable "private_subnet_details" {
  description = "List of cidrs for application subnets"
}

variable "database_subnet_details" {
  description = "List of cidrs for database subnets"
}

variable "private_route_table_details" {
  description = "List of private subnet route tables"
}

variable "public_route_table_details" {
  description = "List of public subnet route tables"
}

variable "database_route_table_details" {
  description = "List of database subnet route tables"
}

variable "security_group_rule" {
  description = "values for the security group rules to be created"
}