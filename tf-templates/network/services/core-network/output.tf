output "vpc_id" {
  description = "ID of the VPC"
  value       = aws_vpc.ecs-vpc.id
}

output "public_subnet_list" {
  description = "List of cidrs for public subnets"
  value       = module.subnet[*].public_subnet_cidr.subnet_id_list
}

output "private_subnet_list" {
  description = "List of cidrs for application subnets"
  value       = module.subnet[*].private_subnet_cidr.subnet_id_list
}

output "database_subnet_list" {
  description = "List of cidrs for database subnets"
  value       = module.subnet[*].database_subnet_cidr.subnet_id_list
}


output "public_route_table_ids" {
  description = "List of public subnet route tables"
  value       = { for k, v in aws_route_table.public_subnet_route_table : v.tags.Name => v.id }
}

output "private_route_table_ids" {
  description = "List of private subnet route tables"
  value       = { for k, v in aws_route_table.private_subnet_route_table : v.tags.Name => v.id }
}

output "database_route_table_ids" {
  description = "List of database subnet route tables"
  value       = { for k, v in aws_route_table.databse_subnet_route_table : v.tags.Name => v.id }
}

output "namespace" {
  description = "Route53 hosted zone for ecs service discovery"
  value       = aws_service_discovery_private_dns_namespace.ecs-namespace.arn
}