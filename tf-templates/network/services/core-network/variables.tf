variable "region" {
  description = "Region to used when creating network components"
}

variable "vpc_cidr_block" {
  description = "VPC CIDR block"
}

variable "enable_dns_hostnames" {
  description = "Enable DNS host name or not"
  type        = bool
}

variable "vpc_name" {
  description = "Name of the ECS VPC"
  type        = string
}

variable "namespace_name" {
  description = "Private hosted zone for service discovery"
  type        = string
}

variable "namespace_tags" {
  description = "Tags for service registry"
  type        = map(string)
}

variable "instance_tenancy" {
  description = "Instance tenancy within the VPC"
  type        = string
}

variable "resource_prefix" {
  description = "prefix for the reources to be created"
  type        = string
}

variable "subnet_cidrs" {
  description = "CIDRs for the subnets to be created"

}