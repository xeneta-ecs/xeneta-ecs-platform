# VPC for the application
resource "aws_vpc" "ecs-vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = var.instance_tenancy
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name = var.vpc_name
  }
}

# Namespace for ecs service discovery
resource "aws_service_discovery_private_dns_namespace" "ecs-namespace" {
  name        = var.namespace_name #"xeneta.local"
  description = "Private hosted zone for service discovery"
  vpc         = aws_vpc.ecs-vpc.id

  tags = var.namespace_tags
}


# Subnets creation 
# 9 subnets : 
#   3 public:
#     To host LB and for high availability
#   6 private subnets:
#     3 private subnets for ECS
#     3 private subnets for RDS
module "subnet" {
  for_each        = var.subnet_cidrs
  source          = "../../modules/subnet/"
  subnet_list     = each.value
  vpc_id          = aws_vpc.ecs-vpc.id
  resource_prefix = var.resource_prefix
}


# Route table to be used by public subnets
resource "aws_route_table" "public_subnet_route_table" {
  for_each = { for subnet in var.subnet_cidrs["public_subnet_cidr"] : subnet.cidr_range => subnet }
  vpc_id   = aws_vpc.ecs-vpc.id

  tags = {
    Name = format("%s-%s-route-table", var.resource_prefix, each.value["name"])
  }
}


# Route table to be used by private subnets
resource "aws_route_table" "private_subnet_route_table" {
  for_each = { for subnet in var.subnet_cidrs["private_subnet_cidr"] : subnet.cidr_range => subnet }
  vpc_id   = aws_vpc.ecs-vpc.id

  tags = {
    Name = format("%s-%s-route-table", var.resource_prefix, each.value["name"])
  }
}

# Route table to be used by databse subnets
resource "aws_route_table" "databse_subnet_route_table" {
  for_each = { for subnet in var.subnet_cidrs["database_subnet_cidr"] : subnet.cidr_range => subnet }
  vpc_id   = aws_vpc.ecs-vpc.id

  tags = {
    Name = format("%s-%s-route-table", var.resource_prefix, each.value["name"])
  }
}
