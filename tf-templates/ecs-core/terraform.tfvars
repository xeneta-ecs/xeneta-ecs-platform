# ******** ECR module ********
####################################################################
# ECR attributes
container_repository_names = ["rates-application", "db-init-job"]
image_tag_mutability       = "MUTABLE"
scan_on_push               = false


# ******** ECS module ********

####################################################################
# ECS attributes
ecs_cluster_name              = "ecs-cluster"
logging                       = "OVERRIDE"
container_insights_setting    = "disabled" # Allowed values: enabled / disabled
cloudwatch_log_group_name     = "ecs-log-group"
ecs_capacity_providers        = ["FARGATE"]
default_ecs_capacity_provider = "FARGATE"



# ******** DB module ********

#################################################################
# RDS attributes
username                 = "postgres"
db_engine                = "postgres"
db_engine_version        = "13.5"
db_instace_class         = "db.t3.micro"
number_of_read_replicas  = 2
backup_retention_period  = 1
backup_window            = "09:46-10:16"
maintenance_window       = "Mon:00:00-Mon:03:00"
delete_automated_backups = true
skip_final_snapshot      = true
publicly_accessible      = false
vpc_name                 = "ecs-vpc"
storage_encrypted        = true

#################################################################
# AWS Secrets Manager attributes
db_secret_name_prefix = "db-secret"

#################################################################
# KMS key attributes
kms_key_name = "db-encryption-key"


# ******** Task definition module ********

###############################################################
# rates-application task definition attributes

rates_app_task_name             = "rates-application-task-definition"
rates_container_definition_name = "rates-application"
rates_container_port            = 3000


###############################################################
# rates-application task definition attributes

db_init_task_name                 = "db-init-application-task-definition"
db_init_container_definition_name = "db-init-application"


###############################################################
# Common attributes of task definitions module
cpu          = 256
memory       = 512
skip_destroy = true