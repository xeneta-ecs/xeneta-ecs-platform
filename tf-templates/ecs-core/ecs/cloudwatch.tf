resource "aws_cloudwatch_log_group" "ecs-log-group" {
  name = var.cloudwatch_log_group_name

  tags = {
    "Name" = var.cloudwatch_log_group_name
  }
}