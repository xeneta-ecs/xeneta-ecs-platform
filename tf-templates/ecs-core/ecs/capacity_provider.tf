resource "aws_ecs_cluster_capacity_providers" "capacity-provider" {
  cluster_name = aws_ecs_cluster.ecs-cluster.name

  capacity_providers = var.ecs_capacity_providers

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = var.default_ecs_capacity_provider
  }

}