variable "ecs-cluster-name" {
  description = "Name of the ECS cluster"
  type        = string
}

variable "logging" {
  description = "Logging stategy in Cloudwatch log group"
  type        = string
}

variable "container_insights_setting" {
  description = "Enable or disable container insights"
  type        = string
}

variable "cloudwatch_log_group_name" {
  description = "Name of the Cloudwatch log group"
  type        = string
}

variable "ecs_capacity_providers" {
  description = "Capacity providers for the ECS cluster"
  type        = list(string)
}

variable "default_ecs_capacity_provider" {
  description = "Capacity provider for ECS"
  type        = string
}