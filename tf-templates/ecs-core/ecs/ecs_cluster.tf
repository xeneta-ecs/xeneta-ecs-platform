resource "aws_ecs_cluster" "ecs-cluster" {
  name = var.ecs-cluster-name

  setting {
    name  = "containerInsights"
    value = var.container_insights_setting
  }

  configuration {
    execute_command_configuration {
      logging = var.logging

      log_configuration {
        cloud_watch_log_group_name = aws_cloudwatch_log_group.ecs-log-group.name
      }
    }
  }

  tags = {
    "Name" = var.ecs-cluster-name
  }

}