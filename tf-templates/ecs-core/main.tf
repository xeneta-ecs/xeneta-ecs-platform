# ECR module
module "ecr" {
  source                     = "./ecr"
  container_repository_names = var.container_repository_names
  image_tag_mutability       = var.image_tag_mutability
  scan_on_push               = var.scan_on_push
  account_id                 = data.aws_caller_identity.current.account_id
}

# ECS module
module "ecs" {
  source                        = "./ecs"
  ecs-cluster-name              = local.ecs_cluster_name
  logging                       = var.logging
  container_insights_setting    = var.container_insights_setting
  ecs_capacity_providers        = var.ecs_capacity_providers
  cloudwatch_log_group_name     = local.cloudwatch_log_group_name
  default_ecs_capacity_provider = var.default_ecs_capacity_provider
}

# RDS module
module "db" {
  source                   = "./database"
  username                 = var.username
  db_engine                = var.db_engine
  db_engine_version        = var.db_engine_version
  db_instace_class         = var.db_instace_class
  number_of_read_replicas  = var.number_of_read_replicas
  backup_retention_period  = var.backup_retention_period
  backup_window            = var.backup_window
  maintenance_window       = var.maintenance_window
  delete_automated_backups = true
  skip_final_snapshot      = true
  publicly_accessible      = false
  vpc_name                 = local.vpc_name
  storage_encrypted        = true
  kms_key_name             = var.kms_key_name
  db_secret_name_prefix    = var.db_secret_name_prefix
  resource_prefix          = local.resource_prefix
}

# Task definitions for applications
module "task-definition" {
  source                            = "./task-definitions"
  db_secret_name_arn                = module.db.db_secret_arn
  rates_app_task_name               = var.rates_app_task_name
  rates_container_definition_name   = var.rates_container_definition_name
  rates_container_port              = var.rates_container_port
  db_init_task_name                 = var.db_init_task_name
  db_init_container_definition_name = var.db_init_container_definition_name
  cpu                               = var.cpu
  memory                            = var.memory
  skip_destroy                      = var.skip_destroy
  resource_prefix                   = local.resource_prefix
  cloudwatch_log_group_name         = local.cloudwatch_log_group_name

  depends_on = [module.db, module.ecs, module.ecr]
}
