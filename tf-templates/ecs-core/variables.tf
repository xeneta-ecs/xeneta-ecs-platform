# ******** ECR module ********
####################################################################

# ECR attributes
variable "container_repository_names" {
  description = "Name of the docker container repository"
  type        = list(string)
}

variable "image_tag_mutability" {
  description = "Whether the image can be replaced with the same tag"
  type        = string
}

variable "scan_on_push" {
  description = "Wherher to scan the image before pushing into the repository"
  type        = bool
}


# ******** ECS module ********
###################################################################
# ECS attributes
variable "ecs_cluster_name" {
  description = "Name of the ECS cluster"
  type        = string
}

variable "logging" {
  description = "Logging stategy in Cloudwatch log group"
  type        = string
}

variable "container_insights_setting" {
  description = "Enable or disable container insights"
  type        = string
}

variable "cloudwatch_log_group_name" {
  description = "Name of the Cloudwatch log group"
  type        = string
}

variable "ecs_capacity_providers" {
  description = "Capacity providers for the ECS cluster"
  type        = list(string)
}

variable "default_ecs_capacity_provider" {
  description = "Capacity provider for ECS"
  type        = string
}


# ******** DB module ********
#################################################################
# RDS attributes
variable "username" {
  default     = "postgres"
  description = "Username to use in RDS"
}


variable "db_engine" {
  default     = "postgres"
  description = "DB engine to be used"
}

variable "db_engine_version" {
  default     = "13.5"
  description = "DB engine version to be used"
}

variable "db_instace_class" {
  default     = "db.t3.micro"
  description = "Instance class type of the RDS instances"
}

variable "number_of_read_replicas" {
  default     = 2
  description = "Number of read replicas for DB"
}

variable "backup_retention_period" {
  description = "The days to retain backups for"
}

variable "backup_window" {
  description = "The daily time range during which automated backups are created"
}

variable "maintenance_window" {
  description = "The window to perform maintenance in"
}

variable "delete_automated_backups" {
  description = "Whether to remove automated backups immediately after the DB instance is deleted"
}

variable "skip_final_snapshot" {
  description = "Whether a final DB snapshot is created before the DB instance is deleted"
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
}

variable "vpc_name" {
  description = "Short name of the VPC"
  default     = "ecs-vpc"
}

variable "storage_encrypted" {
  description = " Specifies whether the DB instance is encrypted"
}

#################################################################
# AWS Secrets Manager attributes
# variable "db_password_name" {
#   description = "Secret name for the secret manager to store db password"
#   type        = string
# }

# variable "db_endpoint_name" {
#   description = "Secret name for the secret manager to store db endpoint"
#   type        = string
# }

variable "db_secret_name_prefix" {
  description = "Name prefix of the secret manager secret name"
  type        = string
}

#################################################################
# KMS key attributes
variable "kms_key_name" {
  description = "RDS encryption key"
}


# ******** Task definition module ********
###############################################################
# rates-application task definition attributes
variable "rates_app_task_name" {
  description = "Name of the rates application task definition"
  type        = string
}

variable "rates_container_definition_name" {
  description = "Image name of the rates app"
  type        = string
}


variable "rates_container_port" {
  description = "rates-application container port"
  type        = number
}


###############################################################
# db-initialization task definition attributes

variable "db_init_task_name" {
  description = "Name of the db init application task definition"
  type        = string
}

variable "db_init_container_definition_name" {
  description = "Task definition name of the db init job"
  type        = string
}


###############################################################
# Common attributes of task-definition module

variable "cpu" {
  description = "CPU milicores needed to run the conatiner"
}

variable "memory" {
  description = "Memory limit allocated to the container"
}

variable "skip_destroy" {
  description = "Whether to skip destroying previous version when creating a new one"
  type        = bool
}

# variable "db_password_name" {
#   description = "Secret name for the secret manager to store db password"
#   type        = string
# }

# variable "db_endpoint_name" {
#   description = "Secret name for the secret manager to store db endpoint"
#   type        = string
# }
# variable "db_secret_name_prefix" {
#   description = "Name prefix of the secret manager secret name"
#   type = string
# }
