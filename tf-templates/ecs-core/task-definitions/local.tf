locals {
  rates_image_uri   = format("%s:latest", data.aws_ecr_repository.rates-repository.repository_url)
  db_init_image_uri = format("%s:latest", data.aws_ecr_repository.db-init-job-repository.repository_url)
}
