# Task definition for rates application

resource "aws_ecs_task_definition" "rates-task-definition" {
  family                   = var.rates_app_task_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  skip_destroy             = var.skip_destroy
  task_role_arn            = aws_iam_role.ecs-task-role.arn
  execution_role_arn       = aws_iam_role.ecs-task-execution-role.arn
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = jsonencode([
    {
      name      = var.rates_container_definition_name
      image     = local.rates_image_uri
      essential = true

      secrets = [
        {
          valueFrom = format("%s:db_password::", var.db_secret_name_arn)
          name      = "PGPASSWORD"
        },
        {
          valueFrom = format("%s:db_endpoint::", var.db_secret_name_arn)
          name      = "db_endpoint"
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.cloudwatch_log_group_name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "rates-"
        }
      }

      portMappings = [
        {
          containerPort = var.rates_container_port
        }
      ]
    }
  ])

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  tags = {
    "Name" = var.rates_app_task_name
  }

  depends_on = [aws_iam_role.ecs-task-execution-role, aws_iam_role.ecs-task-role]
}


# Task definition for database initialization job

resource "aws_ecs_task_definition" "db-init-task-definition" {
  family                   = var.db_init_task_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  skip_destroy             = var.skip_destroy
  task_role_arn            = aws_iam_role.ecs-task-role.arn
  execution_role_arn       = aws_iam_role.ecs-task-execution-role.arn
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = jsonencode([
    {
      name      = var.db_init_container_definition_name
      image     = local.db_init_image_uri
      essential = true

      secrets = [
        {
          valueFrom = format("%s:db_password::", var.db_secret_name_arn)
          name      = "PGPASSWORD"
        },
        {
          valueFrom = format("%s:db_endpoint::", var.db_secret_name_arn)
          name      = "db_endpoint"
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.cloudwatch_log_group_name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "db-init-"
        }
      }


    }
  ])

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  tags = {
    "Name" = var.db_init_container_definition_name
  }

  depends_on = [aws_iam_role.ecs-task-execution-role, aws_iam_role.ecs-task-role]
}