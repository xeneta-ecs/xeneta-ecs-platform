locals {
  account_name      = data.aws_iam_account_alias.current.account_alias
  account_name_list = split("-", local.account_name)
  business_unit     = local.account_name_list[1]
  project_name      = local.account_name_list[2]
  environment       = local.account_name_list[3]
  resource_prefix   = format("%s-%s-%s", local.business_unit, local.project_name, local.environment)

  vpc_name                  = format("%s-%s", local.resource_prefix, var.vpc_name)
  ecs_cluster_name          = format("%s-%s", local.resource_prefix, var.ecs_cluster_name)
  cloudwatch_log_group_name = format("%s-%s", local.resource_prefix, var.cloudwatch_log_group_name)
}