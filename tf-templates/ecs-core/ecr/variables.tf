variable "container_repository_names" {
  description = "Name of the docker container repositories"
  type        = list(string)
}

variable "image_tag_mutability" {
  description = "Whether the image can be replaced with the same tag"
  type        = string
}

variable "scan_on_push" {
  description = "Wherher to scan the image before pushing into the repository"
  type        = bool
}

variable "account_id" {
  description = "AWS account ID"
  type        = string
}