locals {
  kms_key_name          = format("%s-%s", var.resource_prefix, var.kms_key_name)
  db_identifier         = format("%s-db", var.resource_prefix)
  db_subnet_ids         = data.aws_subnet_ids.db_subnet_ids.ids
  db_security_group_ids = data.aws_security_groups.db_security_group_ids.ids
  db_az_zones           = [for subnet in data.aws_subnet.db_subnet : subnet.availability_zone]
}