# DB master instance
resource "aws_db_instance" "db" {
  allocated_storage        = 5
  engine                   = var.db_engine
  engine_version           = var.db_engine_version
  instance_class           = var.db_instace_class
  identifier               = local.db_identifier
  username                 = var.username
  db_subnet_group_name     = aws_db_subnet_group.db-subnet-group.name
  password                 = random_string.postgres-db-password.result
  backup_retention_period  = var.backup_retention_period
  backup_window            = var.backup_window
  maintenance_window       = var.maintenance_window
  delete_automated_backups = var.delete_automated_backups
  vpc_security_group_ids   = local.db_security_group_ids
  skip_final_snapshot      = var.skip_final_snapshot
  publicly_accessible      = var.publicly_accessible
  storage_encrypted        = var.storage_encrypted
  availability_zone        = local.db_az_zones[length(local.db_az_zones) - 1]
  kms_key_id               = aws_kms_key.db-encryption-key.arn

  tags = {
    "Name" = format("%s-db-master", var.resource_prefix)
  }
}

# DB read replicas
resource "aws_db_instance" "db-read-replica" {
  count                  = var.number_of_read_replicas
  replicate_source_db    = aws_db_instance.db.identifier
  identifier             = format("%s-read-replica-%s", local.db_identifier, count.index + 1)
  instance_class         = var.db_instace_class
  vpc_security_group_ids = local.db_security_group_ids
  storage_encrypted      = var.storage_encrypted
  skip_final_snapshot    = var.skip_final_snapshot
  kms_key_id             = aws_kms_key.db-encryption-key.arn
  availability_zone      = local.db_az_zones[count.index]

  tags = {
    "Name" = format("%s-db-replica-%s", var.resource_prefix, count.index + 1)
  }
}

# Generates a random password for RDS
resource "random_string" "postgres-db-password" {
  length  = 32
  upper   = true
  numeric = true
  special = false
}

# The subnets to be used for DB instances 
resource "aws_db_subnet_group" "db-subnet-group" {
  name       = format("%s-subnet-group", var.resource_prefix)
  subnet_ids = local.db_subnet_ids

  tags = {
    Name = format("%s-subnet-group", var.resource_prefix)
  }
}
