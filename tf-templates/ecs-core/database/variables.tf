#################################################################
# RDS attributes
variable "username" {
  default     = "postgres"
  description = "Username to use in RDS"
}


variable "db_engine" {
  default     = "postgres"
  description = "DB engine to be used"
}

variable "db_engine_version" {
  default     = "13.5"
  description = "DB engine version to be used"
}

variable "db_instace_class" {
  default     = "db.t3.micro"
  description = "Instance class type of the RDS instances"
}

variable "number_of_read_replicas" {
  default     = 2
  description = "Number of read replicas for DB"
}

variable "backup_retention_period" {
  description = "The days to retain backups for"
}

variable "backup_window" {
  description = "The daily time range during which automated backups are created"
}

variable "maintenance_window" {
  description = "The window to perform maintenance in"
}

variable "delete_automated_backups" {
  description = "Whether to remove automated backups immediately after the DB instance is deleted"
}

variable "skip_final_snapshot" {
  description = "Whether a final DB snapshot is created before the DB instance is deleted"
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
}

variable "vpc_name" {
  description = "Short name of the VPC"
  default     = "ecs-vpc"
}

variable "storage_encrypted" {
  description = " Specifies whether the DB instance is encrypted"
}

variable "db_secret_name_prefix" {
  description = "Secret name for the secret manager"
  type        = string
}

#################################################################
# KMS key attributes
variable "kms_key_name" {
  description = "RDS encryption key"
}

#################################################################
# Common attributes
variable "resource_prefix" {
  type = string
}