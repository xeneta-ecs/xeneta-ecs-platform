data "aws_iam_account_alias" "current" {}

data "aws_vpc" "ecs-vpc" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_name]
  }
}

# Get the database subnet ids list
data "aws_subnet_ids" "db_subnet_ids" {
  vpc_id = data.aws_vpc.ecs-vpc.id
  filter {
    name   = "tag:type"
    values = ["database"]
  }
}

# Get each subnet object in order to retrieve it's availability zone via locals 
data "aws_subnet" "db_subnet" {
  for_each = data.aws_subnet_ids.db_subnet_ids.ids
  id       = each.key
}

# Get the database security group ids list
data "aws_security_groups" "db_security_group_ids" {
  tags = {
    type = "database"
  }
}

