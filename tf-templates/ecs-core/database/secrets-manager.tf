# resource "aws_secretsmanager_secret" "db-password" {
#   name        = var.db_password_name
#   description = "Secret for password for DB"

#   tags = {
#     Name = var.db_password_name
#   }

# }

# resource "aws_secretsmanager_secret" "db-endpoint" {
#   name        = var.db_endpoint_name
#   description = "Secret for endpoint of DB"

#   tags = {
#     Name = var.db_endpoint_name
#   }

# }

# resource "aws_secretsmanager_secret_version" "db-password-version" {
#   secret_id     = aws_secretsmanager_secret.db-password.id
#   secret_string = random_string.postgres-db-password.result
# }

# resource "aws_secretsmanager_secret_version" "db-endpoint-version" {
#   secret_id     = aws_secretsmanager_secret.db-endpoint.id
#   secret_string = aws_db_instance.db.address
# }




resource "aws_secretsmanager_secret" "db-secret" {
  name_prefix = var.db_secret_name_prefix
  description = "Secret for password for DB"

  tags = {
    Name = var.db_secret_name_prefix
  }

}

resource "aws_secretsmanager_secret_version" "db-secret-version" {
  secret_id = aws_secretsmanager_secret.db-secret.id
  secret_string = jsonencode({
    db_endpoint = aws_db_instance.db.address
    db_username = aws_db_instance.db.username
    db_password = random_string.postgres-db-password.result
  })
}
