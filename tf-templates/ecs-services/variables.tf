# rates-application ECS service attributes
##########################################################################################
variable "ecs_rates_service_name" {
  description = "Name of the ECS rates application service"
  type        = string
}

variable "rates_task_definition_name" {
  description = "Name of the rates-app task definition"
  type        = string
}

variable "rates_app_desired_count" {
  description = "Deisred count of rates-app task instances"
  type        = number
}

variable "rates_app_container_definition_name" {
  description = "Container name of the rates-app the task defintion"
  type        = string
}

variable "rates_app_container_port" {
  description = "Container port of rates-app that is accpeting traffic"
  type        = number
}

variable "rates_app_deployment_maximum_percent" {
  description = "maximum percentage of containers to be running on a rolling update deployment for rates app"
  type        = number
}

variable "rates_app_deployment_minimum_healthy_percent" {
  description = "Minimum percentage of containers to be running on a rolling update deployment for rates app"
  type        = number
}


# db-init-job ECS service attributes
##########################################################################################

variable "ecs_db_init_service_name" {
  description = "Name of the ECS db init service"
  type        = string
}

variable "db_init_task_definition_name" {
  description = "Name of the db init task definition"
  type        = string
}

variable "db_init_desired_count" {
  description = "Deisred count of task instances"
  type        = number
}


# rates-application ECS LB attributes
##########################################################################################

variable "rates_app_lb_name" {
  description = "Name of the load balancer"
  type        = string
}

variable "rates_app_lb_listener_port" {
  description = "The loadbalancer port listening to the inbound traffic"
  type        = number
}

variable "rates_app_lb_target_group_name" {
  description = "Name of the Target group"
  type        = string
}

variable "rates_app_lb_target_group_port" {
  description = "Target group port"
  type        = number
}

variable "rates_app_lb_healthy_threshold" {
  description = "Healthy threshold for consecutive healthchecks"
  type        = number
}

variable "rates_app_lb_healthcheck_interval" {
  description = "Health check interval period in seconds"
  type        = number
}

variable "rates_app_lb_healthcheck_path" {
  description = "Healthcheck path for LB"
  default     = "/"
}

variable "rates_app_lb_healthcheck_timeout" {
  description = "value"
}

variable "rates_app_lb_unhealthy_threshold" {
  description = "Unhealthy threshold for consecutive healthchecks"
  type        = number
}



# rates application ECS autoscaling attributes
##########################################################################################

variable "rates_app_max_capacity" {
  description = "Maximum number of instances of task"
  type        = number
}

variable "rates_app_min_capacity" {
  description = "Minimum number of instances of task"
  type        = number
}

variable "rates_app_target_tracking_policy_name" {
  description = "Name of the ecs autoscaling policy"
  type        = string
}

variable "rates_app_avg_cpu_threshold" {
  description = "Average CPU thrshold to trigger scale out policy"
  type        = number
}

variable "rates_app_avg_cool_down_time" {
  description = "Time for cooldown period for scale down"
  type        = number
}


# Namespace variables
##########################################################################################

variable "namespace_name" {
  description = "Namespace for the ECS"
  type        = string
}


# Common variables
##########################################################################################

variable "ecs_cluster_name" {
  description = "ID of the ECS cluster"
  type        = string
}

# variable "application_subnets" {
#   description = "Subnets where to deploy the service"
#   type        = list(string)
# }

# variable "application_security_groups" {
#   description = "Security groups applied to the ECS service"
#   type        = list(string)
# }

variable "launch_type" {
  description = "Capacity provider type for deployment"
  type        = string
}

# variable "ecs_cluster_vpc_id" {
#   description = "ECS VPC ID"
#   type        = string
# }

# variable "public_subnets" {
#   description = "Subnets where to deploy the service"
#   type        = list(string)
# }

# variable "public_security_groups" {
#   description = "Security groups applied to the ECS service"
#   type        = list(string)
# }

variable "vpc_name" {
  description = "Short name of the VPC"
  default     = "ecs-vpc"
}