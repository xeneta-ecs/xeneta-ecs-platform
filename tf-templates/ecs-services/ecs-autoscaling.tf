# ECS rates-application autoscaling
resource "aws_appautoscaling_target" "rates-app-ecs-target" {
  max_capacity       = var.rates_app_max_capacity
  min_capacity       = var.rates_app_min_capacity
  resource_id        = "service/${local.ecs_cluster_name}/${aws_ecs_service.ecs-rates-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

}

# rates-application autoscaling policy
resource "aws_appautoscaling_policy" "ecs_policy" {
  name               = var.rates_app_target_tracking_policy_name
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.rates-app-ecs-target.resource_id
  scalable_dimension = aws_appautoscaling_target.rates-app-ecs-target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.rates-app-ecs-target.service_namespace

  target_tracking_scaling_policy_configuration {
    target_value      = var.rates_app_avg_cpu_threshold
    scale_in_cooldown = var.rates_app_avg_cool_down_time
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }
}