data "aws_iam_account_alias" "current" {}

data "aws_vpc" "ecs-vpc" {
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}

data "aws_service_discovery_dns_namespace" "ecs-namespace" {
  name = var.namespace_name
  type = "DNS_PRIVATE"
}

# Retrieve subnet information
data "aws_subnet_ids" "application_subnet_ids" {
  vpc_id = data.aws_vpc.ecs-vpc.id
  filter {
    name   = "tag:type"
    values = ["private"]
  }
}

data "aws_subnet_ids" "public_subnet_ids" {
  vpc_id = data.aws_vpc.ecs-vpc.id
  filter {
    name   = "tag:type"
    values = ["public"]
  }
}

# Retrieve security groups information
data "aws_security_groups" "application_security_group_ids" {
  tags = {
    type = "private"
  }
}

data "aws_security_groups" "public_security_group_ids" {
  tags = {
    type = "public"
  }
}


# Retrieve ECS cluster ID
data "aws_ecs_cluster" "ecs-cluster" {
  cluster_name = local.ecs_cluster_name
}


# Retrieve task definition information
data "aws_ecs_task_definition" "rates-app-task-defintion" {
  task_definition = var.rates_task_definition_name
}

data "aws_ecs_task_definition" "db-init-task-defintion" {
  task_definition = var.db_init_task_definition_name
}