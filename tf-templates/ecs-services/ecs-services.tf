# ECS rates-application service
resource "aws_ecs_service" "ecs-rates-service" {
  name            = var.ecs_rates_service_name
  cluster         = data.aws_ecs_cluster.ecs-cluster.id
  task_definition = data.aws_ecs_task_definition.rates-app-task-defintion.arn
  desired_count   = var.rates_app_desired_count

  load_balancer {
    target_group_arn = aws_lb_target_group.rates-app-ip-target-group.arn
    container_name   = var.rates_app_container_definition_name
    container_port   = var.rates_app_container_port
  }

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }

  network_configuration {
    subnets          = data.aws_subnet_ids.application_subnet_ids.ids
    assign_public_ip = false
    security_groups  = data.aws_security_groups.application_security_group_ids.ids
  }

  service_registries {
    registry_arn = aws_service_discovery_service.discovery-service.arn
  }

  deployment_maximum_percent         = var.rates_app_deployment_maximum_percent
  deployment_minimum_healthy_percent = var.rates_app_deployment_minimum_healthy_percent
  launch_type                        = var.launch_type

  depends_on = [
    aws_ecs_service.ecs-db-init-service
  ]
}



# ECS db-init-job service
resource "aws_ecs_service" "ecs-db-init-service" {
  name            = var.ecs_db_init_service_name
  cluster         = data.aws_ecs_cluster.ecs-cluster.id
  task_definition = data.aws_ecs_task_definition.db-init-task-defintion.arn
  desired_count   = var.db_init_desired_count

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }

  network_configuration {
    subnets          = data.aws_subnet_ids.application_subnet_ids.ids
    assign_public_ip = false
    security_groups  = data.aws_security_groups.application_security_group_ids.ids
  }

  launch_type = var.launch_type
}

