# rates-application ECS service attributes
##########################################################################################

ecs_rates_service_name              = "rates-application-service"
rates_task_definition_name          = "rates-application-task-definition"
rates_app_container_definition_name = "rates-application"

rates_app_desired_count                      = 3
rates_app_container_port                     = 3000
rates_app_deployment_maximum_percent         = 200
rates_app_deployment_minimum_healthy_percent = 50

# db-init-job ECS service attributes
##########################################################################################

ecs_db_init_service_name     = "db-init-service"
db_init_task_definition_name = "db-init-application-task-definition"

db_init_desired_count              = 1
db_init_deployment_maximum_percent = 200


# rates-application ECS LB attributes
##########################################################################################
rates_app_lb_name              = "rates-application-alb"
rates_app_lb_target_group_name = "rates-app-alb-target-group"
rates_app_lb_healthcheck_path  = "/"

rates_app_lb_listener_port        = 80
rates_app_lb_target_group_port    = 3000
rates_app_lb_healthy_threshold    = 3
lb_healthcheck_interval           = 5
rates_app_lb_healthcheck_interval = 10
rates_app_lb_healthcheck_timeout  = 5
rates_app_lb_unhealthy_threshold  = 3

# rates application ECS autoscaling attributes
##########################################################################################
rates_app_target_tracking_policy_name = "ecs-rates-app-scaling-policy"

rates_app_max_capacity       = 4
rates_app_min_capacity       = 1
rates_app_avg_cpu_threshold  = 75
rates_app_avg_cool_down_time = 300

# Common variables
##########################################################################################
ecs_cluster_name = "ecs-cluster"
vpc_name         = "ecs-vpc"
namespace_name   = "xeneta.local"
launch_type      = "FARGATE"