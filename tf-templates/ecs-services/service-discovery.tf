resource "aws_service_discovery_service" "discovery-service" {
  name = var.ecs_rates_service_name

  dns_config {
    namespace_id = data.aws_service_discovery_dns_namespace.ecs-namespace.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

}