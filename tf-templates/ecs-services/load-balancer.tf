# Load balancer for rates-application
resource "aws_lb" "rates-app-application-lb" {
  name               = var.rates_app_lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = data.aws_security_groups.public_security_group_ids.ids
  subnets            = data.aws_subnet_ids.public_subnet_ids.ids

  enable_deletion_protection = false

  tags = {
    Name = var.rates_app_lb_name
  }
}


# LB listener for rates-application
resource "aws_lb_listener" "rates-app-application-lb-listener" {
  load_balancer_arn = aws_lb.rates-app-application-lb.arn
  port              = var.rates_app_lb_listener_port
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.rates-app-ip-target-group.arn
  }
}

# LB target group for rates-application 
resource "aws_lb_target_group" "rates-app-ip-target-group" {
  name        = var.rates_app_lb_target_group_name
  port        = var.rates_app_lb_target_group_port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.ecs-vpc.id

  health_check {
    healthy_threshold   = var.rates_app_lb_healthy_threshold    #3
    interval            = var.rates_app_lb_healthcheck_interval #5
    port                = var.rates_app_lb_target_group_port
    path                = var.rates_app_lb_healthcheck_path #"/"
    protocol            = "HTTP"
    timeout             = var.rates_app_lb_healthcheck_timeout #10
    unhealthy_threshold = var.rates_app_lb_unhealthy_threshold #3
  }
}

