# XENETA-OPERATIONS-TASK CHANGELOG

## Minor Release 1.1.0
Added:
- README.md
- CASE_STUDY.md


## Major Release 1.0.0
Added:
- network
- ECS & ECR as a PaaS
- RDS for DbaS
- AWS Secret Manger for managing secrets.
