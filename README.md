# **XENETA Operations Task**

## **Introduction**

This project was created for implementing practical solution for [xeneta-operational-task](https://github.com/xeneta/operations-task). The solution is imeplemented on AWS cloud platform's Elastic Container Service. This project uses GitLab CI/CD to provision the infrastructure and deploy the application.

## **Tools Used**

- Terraform (v1.3.1)
- AWS (ECS, ECR, RDS, Secrets Manager, CloudWatch, VPC)
- GitLab
- Docker

## Prerequisites

- Require an AWS account.
- An IAM user with administrator permissions. (Only programmatic access required).
- Require an GitLab account (You can create a free account from [here](https://gitlab.com/users/sign_up)).
- **Important**: Your AWS account must have an ALIAS in the format **"aws-{Business_Unit_Name}-{Project_Name}-{Environment}"**

**Note:** This suggested AWS account naming convention is used for naming resources that gets created in the AWS infrastructure and deploying the ECS platform to separate environments.

You can create an alias for your AWS account in IAM console as in below screenshot:

Here business_unit_name=home, project_name=lahiru, environment=dev

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/account_alias.JPG)


## Advantages of using the AWS Organization

- For an enterprise organization having many products can use AWS organization to manage their accounts. The suggested **naming convention** can be used to have an single AWS account for each product based on it's environment.
- Having one account for one product's environment minimizes the blast radius on a breach.
- It's easy to govern all the accounts in the organization using SCPs.
- Can govern the cost from a central point.

## How to Deploy the Application to Mutiple Environments

As per the AWS account naming convention used in above Prerequisites section, one AWS account is considered as one environment only. Here if you need to deploy the application to another environment (another AWS account) you have to re-configure the AWS credentials in GitLab CI/CD. 

If you are using AWS Organization, you can switch the account from master account with assume roles programmatically using Boto3 API. (This requires a new utility script to be added)

## Terraform Backend

GitLab's recently introduced Infrastructure feature is used to store the Terraform state in this setup.

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/tf_state.JPG)

## **Architecture**

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/xeneta_architecture.jpg)

### Description:
- ECS was used as application host plafform because it provides high availability & low cost. ECS is useful as a microservice platform with it's native integrations to other services in AWS.
- Fargate was used here as the capacity provider since there is only one application to be hosted and it's not a long running application as well. 
-  AWS secret manager was used as Database credential management tool due to it's native integration with ECS
- Before deploying "rates" application, another short lived task is initiated in ECS to initialize the PostgreSQL DB.
- 3 Availability zones are provided to maintain HA for the application.
- DB instance have 2 replicas to manage it's high availability.
- KMS is used to encrypt the data in RDS instances.
- To restrict traffic to inner layers, application layer SG is only accepting traffic from ALB and, DB layer SG is only accepting traffic from ALB.
- 3 NAT GWs are used to provide high availability to each AZ and, each subnet in each AZ route traffic to internet with the corresponding NAT GW within it's AZ.
- 3 Replicas of the application will be deployed for HA of the application.
- All the resources created in the AWS account will have tags and and names associated with the account's alias prefix.
- The setup is set to be provisioned in us-east-1 only with the current configuration parameters provided in the network module's tfvars config and the GitLab CI job specs.


## **GitLab Pipeline Flow**

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/gitlab-flow.jpg)


## Steps to provision the infrastructure and deploy the application

1. Login to your GitLab account.
2. Create a new project.
3. Add this repository's code into your new project.
4. Create a personal access token in GitLab 
5. In the left side pane, select *Settings>CI/CD*.
6. Select Expand in the Variables section. There add below variables and their corresponding values into that section.
    - [ ] AWS_ACCESS_KEY_ID (This is the the access key id of the programmatic user created in prerequisites section) 
    - [ ] AWS_SECRET_ACCESS_KEY (This is the the secret access key id of the programmatic user created in prerequisites section)
    - [ ] PROJECT_ID (This is the GitLab project ID)
    - [ ] TF_PASSWORD (This is the *personal access token* you have to create in GitLab with the permission to api, read_api, read_user, read_repository, write_repository, read_registry, write_registry)
    - [ ] TF_USERNAME (Username of your GitLab account)
    - [ ] AWS_ACCOUNT_ID

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/env_variables.JPG)

7. Click on CI/CD=>Pipelines on left side panel and click on Run the pipeline. (No key/value parameters needed).
8. Each apply stage is set to run manually after the user is satisfied with changes getting applied on Plan stages. Therefore click on trigger button in each apply job to trigger them.
9. After the pipline succeeds, go to AWS console use the DNS of the ALB created to browse the output from the REST API. ALB is listening on port 80 and traffic will be forwarded to port 3000 in the application.

**Note:** 
- Here all other variables except *TF_USERNAME*, and *PROJECT_ID* should be masked variables to improve security.
- *TF_USERNAME*, *PROJECT_ID*, and *PROJECT_ID* is used to store Terraform state in GitLab.

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/deployment.JPG)


## Steps to decommission the infrastructure

1. Click on CI/CD=>Pipelines on left side panel.
2. Run the pipeline with variable *TF_DESTROY* set to *true*

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/decommission.JPG)





