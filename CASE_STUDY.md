# **Data ingestion pipeline**

![Image](https://gitlab.com/xeneta-ecs/xeneta-ecs-platform/-/raw/main/images/data_ingestion_pipeline.jpg)

### **Workflow:**

1. AWS Kinesis Firehose is used to stream the incoming unstructured data from various producers into Raw data S3 bucket.
    - [ ] *Amazon Kinesis Firehose was selected to consume the data incoming from data producers continuously because it is able to scale as the incoming data grows and will reliably deliver the unstructured data to the S3 bucket.*
2. Upon new object insertions to the raw data S3 bucket, the Lambda function will get triggered in order to transform raw data into menainful records.
    - [ ] *The reason to chosse AWS Lambda function here is because it can scale it's number of instances as the number of event triggers.*
3. After tranforming the data, the lambda function will push the records into the SQS.
    - [ ] This is to queue the large number of POST requests that are getting fired from Lambda instances to insert data to S3 bucket.
4. Here the Lambda function polls messages from SQS.
    - [ ] The Lambda function will gracefully consume batches of data to insert into S3 bucket.
5. Lambda function sends the polled records into relevant paths in S3 bucket (data lake).
    - [ ] The Lambda function will send the structured data to relevant paths so the AWS Glue can detect the structure of tables within S3 bucket.
6. AWS Glue is connected with data lake to insert batches of data into the high available PostgreSQL Database periodically.
    - [ ] AWS Glue is selected here because it is a managed service that is integrated with RDS to insert batches of data.
7. AWS Glue inserts batches of data periodically that are newly inserted into the S3 bucket (data lake).
8. AWS Glue also connected with AWS Redshift for inserting the data for future analytics.

**Note**: We can use VPC endpoints for lambda functions to communicate with S3 buckets for improved security and speed.

## **Monitoring**

- For granular level monitoring, we can integrate all the components with DataDog.


## **Additional questions**

1. 
    - [ ] Will need to increase the CPU, memory and disk performance in the Database master instance. 
    - [ ] Also *COPY* command in PostgreSQL can be used to insert data from csv file instead of bulk insert command. 
    - [ ] Another way to reduce processing time is to reduce the amount of records in a batch that is inserted per transaction in DB.
2. Lambda functions can be deployed via rolling deployments.





 

